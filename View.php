<?php
class View
{
    const TEMPLATES_PATH = "templates/";

    private $file;

    public function __construct($template, $variables) {
        $this->file = self::TEMPLATES_PATH . $template;

        // These will be available in template through reference to this class
        // e.g.  print $this->pageTitle 
        foreach ($variables as $key => $var) {
            // Should really have custom get/set methods to ensure that
            // none of these values overwrite $this->file, maybe by placing 
            // everything into an array
            $this->$key = $var;
        }
    }

    public function render($addBaseTemplate = true) {
        ob_start();
        include($this->file);
        $content = ob_get_clean();

        if ($addBaseTemplate) {
            $fullPage = new View('base.php', array('content' => $content));
            $content = $fullPage->render(false);
        }
        return $content;
    }
}