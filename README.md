# Beer application demo - Ciarán Phillips

## Installation

* Clone the repo
* Move config.json.template to config.json, and add an api key
* Run using the builtin php dev server: `php -S localhost:8000`
* If in a VM, you might need to attach to 0.0.0.0 instead of localhost

## Running tests

* Install phpunit with `php composer.phar install`
* Run tests with `php vendor/phpunit/phpunit/phpunit tests`

## Comments

I probably spent more time on this than I was meant to, as in the interview I think I was given the impression it should just be an hour or two, but honestly I'm not sure what I would have produced within that time. I probably should have simplified what I was building but I guess I was overly optimistic.

Some things here are still pretty ugly:

* Router class is doing the job of controller too, and is just a little muddled
* I tried to separate out dependencies, so they're injected through the constructor, but it's not a nice way of handling things
* Should really have autoloading. Maybe it would only take a minute to set up, but I guess it's a while since I started a php project from scratch...
* The View class is buggy and can accidentally overwrite fields it relies on for operation with template variables, but that would only take a few minutes to fix really
* Template variables should really be run through a quick sanitization function at the point of rendering, to avoid XSS
* I probably haven't been that careful about validating data coming in from the user through the URL or form. I don't think I've done anything particularly dangerous but I'd want to double check...
* There are a number of places where I'm loading external files or communicating with the API without proper error checking. I should be checking for success, and passing exceptions up the chain til I can display an appropriate error if needed.