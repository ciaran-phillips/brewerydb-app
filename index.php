<?php

require('router.php');

$delegator = new RequestDelegator($_SERVER["REQUEST_URI"]);
if ($delegator->isStaticRequest()) {
    return false;
}
echo $delegator->getResponse();

class RequestDelegator
{
    private $router;

    private $requestUri;

    public function __construct($requestUri, $router = null) {
        $this->requestUri = $requestUri;

        if (is_null($router)) {
            $router = $this->defaultrouter();
        }
        $this->router = $router;
    }

    public function isStaticRequest() {
        return (preg_match('/\.(?:png|jpg|jpeg|gif)$/', $this->requestUri));
    } 

    public function getResponse() {
        // strip GET params
        $pos = strpos($this->requestUri, "?");
        $pos = ($pos === false) ? strlen($this->requestUri) : $pos;
        $uri = substr($this->requestUri, 0, $pos);
        $params = explode('/', $uri);
        // Gets rid of first empty element caused by leading slash in REQUEST_URI
        if (count($params) > 1) {
            array_shift($params);
        }
        return $this->router->process($params);
    }

    private function defaultrouter() {
        return new router();
    }

}

?>