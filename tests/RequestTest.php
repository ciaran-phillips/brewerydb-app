<?php
require_once('BreweryDb/Request.php');

use PHPUnit\Framework\TestCase;

/**
 * @covers Email
 */
final class RequestTest extends TestCase
{
    public function testBuildsCorrectUrlWithNoParams()
    {
        $expectedUrl = "http://api.brewerydb.com/v2/myEndpoint?key=myApiKey";
        $returnData = 'returnData';

        $curl = $this->getMockBuilder(Curl::class)
                         ->setMethods(['get'])
                         ->getMock();

        $curl->expects($this->once())
                 ->method('get')
                 ->with($this->equalTo($expectedUrl))
                 ->will($this->returnValue($returnData));
        
        $cache = $this->failingCacheMock();

        $request = new Request($curl, $cache);
        $request->prepare('myApiKey', 'myEndpoint', array());

        $this->assertEquals(
            $returnData,
            $request->perform()
        );
    }

    public function testBuildsCorrectUrlWithParams()
    {
        // This and the previous test could be combined using PHPUnit's @dataProvider
        $expectedUrl = "http://api.brewerydb.com/v2/myEndpoint?param1=first&param2=second&key=myApiKey";
        $returnData = 'returnData2';

        $curl = $this->getMockBuilder(Curl::class)
                         ->setMethods(['get'])
                         ->getMock();

        $curl->expects($this->once())
                 ->method('get')
                 ->with($this->equalTo($expectedUrl))
                 ->will($this->returnValue($returnData));
        
        $cache = $this->failingCacheMock();

        $request = new Request($curl, $cache);
        $request->prepare('myApiKey', 'myEndpoint', array(
            'param1' => 'first',
            'param2' => 'second'
        ));

        $this->assertEquals(
            $returnData,
            $request->perform()
        );
    }

    public function testLoadsFromCacheIfPresent()
    {
        $returnData = "myReturnData";
        $cache = $this->getMockBuilder(Cache::class)
                            ->setMethods(['get', 'set'])
                            ->getMock();
                            
        $cache->expects($this->any())
                 ->method('get')
                 ->will($this->returnValue($returnData));
        
        
        $curl = $this->getMockBuilder(Curl::class)
                         ->setMethods(['get'])
                         ->getMock();

        $curl->expects($this->never())
                 ->method('get');
        
        $request = new Request($curl, $cache);
        $request->prepare('myApiKey', 'myEndpoint', array());

        $this->assertEquals(
            $returnData,
            $request->perform()
        );
    }

    public function testSetsCacheIfNotPresent()
    {
        $expectedUrl = "http://api.brewerydb.com/v2/myEndpoint?key=myApiKey";
        $returnData = 'returnData';

        $curl = $this->getMockBuilder(Curl::class)
                         ->setMethods(['get'])
                         ->getMock();

        $curl->expects($this->once())
                 ->method('get')
                 ->with($this->equalTo($expectedUrl))
                 ->will($this->returnValue($returnData));
        
        $cache = $this->failingCacheMock();
        $cache->expects($this->any())
                 ->method('set')
                 ->with(
                     $this->equalTo($expectedUrl),
                     $this->equalTo($returnData)
        );

        $request = new Request($curl, $cache);
        $request->prepare('myApiKey', 'myEndpoint', array());

        $this->assertEquals(
            $returnData,
            $request->perform()
        );
    }

    private function failingCacheMock() {
        $cache = $this->getMockBuilder(Cache::class)
                            ->setMethods(['get', 'set'])
                            ->getMock();
                            
        $cache->expects($this->any())
                 ->method('get')
                 ->will($this->returnValue(false));
        return $cache;
    }

}
