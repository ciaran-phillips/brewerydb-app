<?php

require_once('BreweryDb/Api.php');

class RandomBeerGenerator
{
    public function __construct($api = null) {
        if (is_null($api)) {
            $api = $this->defaultApi();
        }
        $this->api = $api;
    }

    public function get() {
        $listing = $this->api->listBeers();
        $numPages = $listing->numberOfPages;
        $randomPageNum = rand(1, $numPages);
        $listing = $this->api->listBeers($randomPageNum);
        $numBeers = count($listing->data);
        $beerNum = rand(0, $numBeers - 1);


        $beer = $listing->data[$beerNum];
        if ($this->validChoice($beer)) {
            return $beer;
        }

        // if our first random beer isn't valid, 
        // check others on the same page (rather than making more API requests)
        foreach ($listing->data as $beer) {
            if ($this->validChoice($beer)) {
                return $beer;
            }
        }
        // If there are no valid beers on this page, try another
        // random page. Hope the loop doesn't just keep going...'
        return $this->get();
    }

    private function validChoice($beer) {
        $hasDescription = trim($beer->description) !== "";
        $hasName = trim($beer->name) !== "";

        return $hasDescription && $hasName;
    }

    private function defaultApi() {
        return new Api();
    }
}