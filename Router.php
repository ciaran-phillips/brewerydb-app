<?php

require('BreweryDb/Api.php');
require('RandomBeerGenerator.php');
require('View.php');

// This class is really doing the job of both a router and controller at the moment,
// so it's a bit ugly. And the routes could be cleaned up a lot - maybe an array
// of route -> actionMethod would be a simple way of improving it, rather than the
// class constants and switch/case
class Router
{
    const FRONT = "";
    const VIEW_BREWERY = "brewery";
    const SEARCH = "search";

    private $api;

    private $randomBeerGen;

    public function __construct($api = null, $randomBeerGen = null) {
        if (is_null($api)) {
            $api = $this->defaultApi();
        }
        $this->api = $api;

        if (is_null($randomBeerGen)) {
            $randomBeerGen = $this->defaultRandomBeerGen();
        }
        $this->randomBeerGen = $randomBeerGen;
    }


    public function process(array $params) {
        $page = array_shift($params);
        switch ($page) {
            case self::FRONT:
                return $this->viewHomepage($params);
                break;
            case self::SEARCH:
                return $this->viewHomepage($params);
                break;
            case self::VIEW_BREWERY:
                return $this->viewBrewery($params);
                break;
        }
    }

    private function viewHomepage($params) {
        $beer = $this->randomBeerGen->get();

        $searchType = $_GET['searchType'];
        $searchString = $_GET['query'];
        $searchResults = $this->api->searchBeers($searchString, $searchType);

        $vars = array(
            "randomBeerName" => $beer->name,
            "randomBeerDescription" => $beer->description,
            "randomBeerBrewery" => $beer->breweries[0]->id,
            "reloadLink" => $_SERVER['REQUEST_URI'],
            "searchResults" => $searchResults->data
        );

        $view = new View('home.php', $vars);
        return $view->render();
    }

    private function viewBrewery($params) {
        $breweryId = $params[0];

        $results = $this->api->listBeersByBrewery($breweryId);
        $vars = array(
            "beers" => $results->data
        );
        $view = new View('brewery.php', $vars);
        return $view->render();
    }

    private function defaultApi() {
        return new Api();
    }

    private function defaultRandomBeerGen() {
        return new RandomBeerGenerator();
    }

}