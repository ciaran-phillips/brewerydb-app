<div>
    <h2>More from this brewery</h2>
    <?php foreach ($this->beers as $beer): ?>
        <div class="media">
            <div class="media-left">
                <?php if (isset($beer->images->icon)): ?>
                    <img class="media-object" src="<?php print $beer->images->icon ?>" alt="">
                <?php elseif (isset($beer->labels->icon)): ?>
                    <img class="media-object" src="<?php print $beer->labels->icon ?>" alt="">
                <?php endif; ?>
            </div>
            <div class="media-body">
                <h4 class="media-heading">
                    <?php print $beer->nameDisplay ?>
                </h4>
                <p>
                    <?php if (!empty($beer->description)): ?>
                        <?php print substr($beer->description, 0, 200) ?>..
                    <?php endif; ?>
                </p>
            </div>
        </div>
    <?php endforeach; ?>
</div>