<div class="panel panel-default">
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-8">
                <h3>
                    <?php print $this->randomBeerName ?>
                </h3>
                <div>
                    <p><?php print $this->randomBeerDescription ?></p>
                </div>

            </div>
            <div class="col-sm-4">
                <a class="btn btn-primary" href="<?php print $this->reloadLink?>">Another Beer</a>
                <a class="btn btn-primary" href="brewery/<?php print $this->randomBeerBrewery ?>">More from this brewery</a>
            </div>
        </div>
    </div>
</div>
<div class="panel panel-default">
    <div class="panel-body">
                
        <form action="search" type="GET">
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Search</label>
                        <input type="search" class="form-control" name="query" id="searchInput" placeholder="Search">
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="radio">
                        <label>
                            <input type="radio" name="searchType" id="searchOptions1" value="beer" checked>
                            Search Beer Names
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="searchType" id="searchOptions12" value="brewery" >
                            Breweries
                        </label>
                    </div></button>
                </div>
                <div class="col-sm-2">
                    
                    <button type="submit" class="btn btn-primary">Search
                </div>
            </div>
        </form>


        <div class="search-results">
            <h2>Search Results</h2>
            <?php foreach ($this->searchResults as $result): ?>
                <div class="media">
                    <div class="media-left">
                        <?php if (isset($result->images->icon)): ?>
                            <img class="media-object" src="<?php print $result->images->icon ?>" alt="">
                        <?php elseif (isset($result->labels->icon)): ?>
                            <img class="media-object" src="<?php print $result->labels->icon ?>" alt="">
                        <?php endif; ?>
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">
                            <?php print $result->nameDisplay ?>
                        </h4>
                        <p>
                            <?php if (!empty($result->description)): ?>
                                <?php print substr($result->description, 0, 200) ?>..
                            <?php endif; ?>
                        </p>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>