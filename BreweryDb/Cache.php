<?php

class Cache 
{

    const CACHE_FILE = __DIR__ . '/cache.json';

    public function get($key) {
        $cacheFile = file_get_contents(self::CACHE_FILE);
        if ($cacheFile == false) {
            $cache = new stdClass();
            file_put_contents(self::CACHE_FILE, json_encode($cache));
        }
        else {
            $cache = json_decode($cacheFile);
        }

        if (isset($cache->$key) && $this->stillRecent($cache->$key->time)) {
            return $cache->$key->data;
        }
        else {
            return false;
        }
    }

    public function set($key, $data) {
        $cacheFile = file_get_contents(SELF::CACHE_FILE);
        $cache = json_decode($cacheFile);
        $cache->$key = new stdClass();
        $cache->$key->time = time();
        $cache->$key->data = $data;

        $cacheJson = json_encode($cache);
        file_put_contents(SELF::CACHE_FILE, $cacheJson);
    }

    private function stillRecent($time) {
        return ((time() - $time) < 86400);
    }
}