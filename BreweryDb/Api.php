<?php
require('Request.php');

class Api
{
    private $apiKey;

    private $request;

    public function __construct($apiKey = null, $request = null) {
        if (is_null($apiKey)) {
            $apiKey = $this->defaultApiKey();
        }
        $this->apiKey = $apiKey;

        if (is_null($request)) {
            $request = $this->defaultRequest();
        }
        $this->request = $request;
    }

    public function listBeers($page = 0) {
        $this->request->prepare($this->apiKey, "beers", array(
            "availableId" => 1,
            "p" => $page,
            "withBreweries" => "Y"
        ));
        $response = $this->request->perform();
        
        return json_decode($response);
    }

    public function getBeer($id) {
        $params = array();
        // should have proper validation on input, but this casting at least means anything odd 
        // will change to some garbage number (zero in most cases)
        $id = (int) $id; 
        $this->request->prepare($this->apiKey, "beer/$id", $params);
        $response = $this->request->perform();
        return $response;
    }

    public function listBeersByBrewery($id) {
        $this->request->prepare($this->apiKey, "brewery/$id/beers", array());
        $response = $this->request->perform();
        return json_decode($response);
    }

    public function searchBeers($searchString, $searchType = "beer", $page = 1) {
        if ($searchType !== "beer" && $searchType !== "brewery") {
            $searchType = "beer";
        }

        $params = array(
            "q" => $searchString,
            "type" => $searchType,
            "p" => $page
        );
        $this->request->prepare($this->apiKey, "search", $params);
        $response = $this->request->perform();
        return json_decode($response);
    }

    private function defaultRequest() {
        return new Request();
    }

    private function defaultApiKey() {
        $configFile = file_get_contents(__DIR__ . '/../config.json');
        $conf = json_decode($configFile);
        return $conf->apiKey;
    }
}