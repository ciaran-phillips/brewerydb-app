<?php
require('Curl.php');
require('Cache.php');

class Request
{
    const BASE_URL = "http://api.brewerydb.com/v2/";

    private $url;

    private $cache;

    private $curl;

    public function __construct($curl = null, $cache = null) {
        if (is_null($curl)) {
            $curl = $this->defaultCurl();
        }
        $this->curl = $curl;

        if (is_null($cache)) {
            $cache = $this->defaultCache();
        }
        $this->cache = $cache;
    }

    public function prepare($apiKey, $endpoint, $params = array()) {
        $url = self::BASE_URL . $endpoint;

        $url .= "?";
        if (!empty($params)) {
            $url .= $this->parameterString($params);
        }
        $url .= "key=" . $apiKey;
        $this->url = $url;
    }

    public function perform() {
        // this is pretty ugly. Either the client class should be building a Request and passing it into
        // another class that performs the action, or we should be building the URL on construction.
        // 
        // It really shouldn't be possible to call this method without the Request being in a valid state to attempt it
        if (is_null($this->url)) {
            throw new LogicException("cannot perform request before preparing URL");
        }
        $cached = $this->cache->get($this->url);
        if ($cached) {
            return $cached;
        } 
        else {
            $response = $this->curl->get($this->url);
            $this->cache->set($this->url, $response);
            return $response;
        }
    }

    private function parameterString($params) {
        $paramString = "";
        foreach ($params as $key => $param) {
            $paramString .= urlencode($key) . "=" . urlencode($param) . "&";
        }
        return $paramString;
    }

    private function defaultCurl() {
        return new Curl();
    }

    private function defaultCache() {
        return new Cache();
    }
}